﻿//important info
//https://forum.unity.com/threads/when-to-choose-a-custom-editor-vs-property-drawer.333306/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAttributeDrawer : MonoBehaviour {

    public int a = 0;

    
    [CustomInt("TestCustom int :-)", 5)]
    public int b = 1;
    [Space(10)]
    [Header("Health Settings")]

    public Vector3 pos;
    
}
