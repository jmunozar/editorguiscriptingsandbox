﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveToPosition : MonoBehaviour {

    public Vector3 wantedPosition;
    public string objName;
    public Vector3 wantedRotation;

    public int test = 0;

    //[CustomInt("STES")]
    public int test2 = 1;
    
    void Start() {
        transform.position = wantedPosition;
        gameObject.name = objName;
        transform.eulerAngles = wantedRotation;

    }
}
