﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
public class CustomIntAttribute : PropertyAttribute {

    public string label;
    public GUIStyle labelStyle;
    public float width;
    public int numButtons;

    public CustomIntAttribute(string label, int _numButtons) {
        this.label = label;
        this.numButtons = _numButtons;
        //labelStyle = GUI.skin.GetStyle("minilabel");
        //width = labelStyle.CalcSize(new GUIContent(label)).x;

    }

}
